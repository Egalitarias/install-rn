# Xcode

Open App Store app and install Xcode

Create and deploy an app to an iphone simulator (this will install latest sdk and simulator)

Select SDK "iphoneos"

```
sudo xcode-select --switch /Applications/Xcode.app
```

# Android

Download Android Studio

[Android Studio](https://developer.android.com/studio)

# Brew

[Brew](https://brew.sh/)

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

# Node

```
brew install node
```

## Cocoa Pods

Install cocaopods

[Cocoapods](https://cocoapods.org/)

```
sudo gem install cocoapods
```

# Create a React Native Typescript app

[React Native Typescript](https://reactnative.dev/docs/typescript)

```
npx react-native init MyApp --template react-native-template-typescript
```

# Expo

[Expo](https://docs.expo.io/)

Install expo-cli globally using npm.

```
npm install -g expo-cli
```

```
expo init my-project
cd my-project
npm install
expo start
```
